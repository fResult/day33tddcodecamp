package com.team_c.seat_reservation.repository;

import com.team_c.seat_reservation.domain.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeatRepository extends JpaRepository<Seat, String> {
    Seat findBySeatNo(String position);
}
