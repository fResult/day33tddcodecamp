package com.team_c.seat_reservation.controller;

import com.team_c.seat_reservation.domain.Seat;
import com.team_c.seat_reservation.service.SeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.InputMismatchException;
import java.util.List;

@RestController
@RequestMapping("api/seats")
public class SeatController {

  @Autowired
  SeatService seatService;

  @GetMapping("/all")
  public List<Seat> findAllSeats() {
    return seatService.findAll();
  }


  @PutMapping("/seatNo/{seatNo}")
  public ResponseEntity<String> reserveSeat(@PathVariable String seatNo) {
    ResponseEntity responseEntity = null;
    int status = seatService.reserve(seatNo);

    try {
      if (status == 0) {
          responseEntity = new ResponseEntity<>("Your reservation is successfully.", HttpStatus.OK);
      }
      return responseEntity;

    } catch (InputMismatchException ex) {
      ex.printStackTrace();
      // return http status code 422: Unprocessable Entity
      return new ResponseEntity<>("Your reservation is failed. Please input seat seatNo is correctly.", HttpStatus.UNPROCESSABLE_ENTITY);
    }
  }

}
