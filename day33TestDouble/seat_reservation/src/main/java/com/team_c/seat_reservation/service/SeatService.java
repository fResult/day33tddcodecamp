package com.team_c.seat_reservation.service;

import com.team_c.seat_reservation.domain.Seat;
import com.team_c.seat_reservation.repository.SeatRepository;
import com.team_c.seat_reservation.service.util.SubString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.InputMismatchException;
import java.util.List;


@Service
public class SeatService {

  @Autowired
  SeatRepository seatRepository;

  public List<Seat> findAll() {
    return seatRepository.findAll();
  }


  public int reserve(String seatNo) {

    SubString subString = new SubString();
    Integer seatNoEndsWithNumber = subString.getSeatNoEndsWithNumber(seatNo);
    String seatNo1stLetter = subString.getSeatNoStartsWithAToC(seatNo);
    System.out.println("IsMatches: " + seatNo.matches("[A-Ca-b](?:[1-9]|0[1-9]|10)$") + ", SeatNo: " + seatNo);

    Seat seat = new Seat();

    /**
     * refactoring from this code to new short code in new if statement:
     * if ((seatNo1stLetter.equalsIgnoreCase("a") ||
     *     seatNo1stLetter.equalsIgnoreCase("b") ||
     *     seatNo1stLetter.equalsIgnoreCase("c")) &&
     *      (seatNoEndsWithNumber <= 10 && seatNoEndsWithNumber >= 1)) {
     */
    if (seatNo.matches("[a-cA-C](0[1-9]|10)")) {

      seat.setSeatNo(seatNo);
      seat.setStatus(0);
    } else {

      throw new InputMismatchException();
    }
    return seat.getStatus();
  }

  //  public int reserve(String seatNo) {
  //    Integer seatNoEndsWithNumber = getSeatNoEndsWithNumber(seatNo);
  //    String seatNo1stLetter = getSeatNoStartsWithAToC(seatNo);

  //    Seat seat = seatRepository.findBySeatNo(seatNo);

  //    if ((seatNo1stLetter.equalsIgnoreCase("a") ||
  //      seatNo1stLetter.equalsIgnoreCase("b") ||
  //      seatNo1stLetter.equalsIgnoreCase("c")) &&
  //      (seatNoEndsWithNumber <= 10 && seatNoEndsWithNumber >= 1)) {

  //      seat.setStatus(0);
  //      seatRepository.save(seat);
  //    } else {

  //      throw new InputMismatchException();
  //    }
  //    return seat.getStatus();
  //  }


}
