package com.team_c.seat_reservation.service.util;

public class SubString {

  public int getSeatNoEndsWithNumber(String position) {
    String strNumber = position.substring(1);
    return Integer.valueOf(strNumber);
  }

  public String getSeatNoStartsWithAToC(String position) {
    return position.substring(0, 1);
  }
}
