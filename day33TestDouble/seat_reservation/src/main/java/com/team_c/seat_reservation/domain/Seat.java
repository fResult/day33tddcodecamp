package com.team_c.seat_reservation.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "seats")
public class Seat {
  @Id
  @Column(name = "seat_no")
  String seatNo;

  // if property name is equal to field name in a the table. Need not to config @Column(name = "status")
  @Column(name = "status")
  Integer status;

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getSeatNo() {
    return seatNo;
  }

  public void setSeatNo(String seatNo) {
    this.seatNo = seatNo;
  }

  @Override
  public String toString() {
    return "Seat{" +
      "seatNo='" + seatNo + '\'' +
      ", status=" + status +
      '}';
  }
}
