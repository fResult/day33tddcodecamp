-- CREATE DATABASE seat_reservation;
-- USE seat_reservation

CREATE TABLE seats (
  seat_no  VARCHAR(3),
  status    TINYINT NOT NULL default 1,
  PRIMARY KEY (seat_no)
)


-- INSERT INTO seats (seat_no)
-- VALUES ('A01'),
--  ('A02'),
--  ('A03'),
--  ('A04'),
--  ('A05'),
--  ('A06'),
--  ('A07'),
--  ('A08'),
--  ('A09'),
--  ('A10'),
--  ('B01'),
--  ('B02'),
--  ('B03'),
--  ('B04'),
--  ('B05'),
--  ('B06'),
--  ('B07'),
--  ('B08'),
--  ('B09'),
--  ('B10'),
--  ('C01'),
--  ('C02'),
--  ('C03'),
--  ('C04'),
--  ('C05'),
--  ('C06'),
--  ('C07'),
--  ('C08'),
--  ('C09'),
--  ('C10');