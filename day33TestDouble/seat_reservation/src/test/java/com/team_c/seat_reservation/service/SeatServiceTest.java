package com.team_c.seat_reservation.service;

import com.team_c.seat_reservation.repository.SeatRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.InputMismatchException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)

public class SeatServiceTest {

  @Mock
  SeatRepository seatRepository;

  SeatService seatService;

  // 1. Arrange
  @BeforeEach
  public void setUp() {
    seatService = new SeatService();
  }

  @Test
  public void should_return_status_is_0_when_input_seatNo_start_with_A() {

    int expectedResult = 0;
    // 2. Actual
    int actualResult = seatService.reserve("A01");

    // 3. Assert
    assertEquals(expectedResult, actualResult);
  }

  @Test
  public void should_return_status_is_0_when_input_seatNo_start_with_B() {

    int expectedResult = 0;

    // 2. Actual
    int actualResult = seatService.reserve("B01");

    // 3. Assert
    assertEquals(expectedResult, actualResult);
  }


  @Test
  public void should_return_status_is_0_when_input_seatNo_start_with_C() {

    int expectedResult = 0;

    // 2. Actual
    int actualResult = seatService.reserve("C01");

    // 3. Assert
    assertEquals(expectedResult, actualResult);
  }

  @Test
  public void should_throw_InputMismatchException_when_input_seatNo_start_without_A_to_C() {

    // 2. Actual
    Executable actualResult = () -> {
      seatService.reserve("D01");
    };

    // 3. Assert
    assertThrows(InputMismatchException.class, actualResult);
  }

  @Test
  public void should_throw_InputMismatchException_when_input_seat_seatNo_end_with_moreThan10() {

    // 2. Actual
    Executable actualResult = () -> {
      seatService.reserve("A11");
    };

    // 3. Assert
    assertThrows(InputMismatchException.class, actualResult);
  }

  @Test
  public void should_return_status_0_when_input_seatNo_end_with_from_1_to_10() {
    // 1. Arrange
    Integer expectedResult = 0;

    // 2. Actual
    Integer actualResult = seatService.reserve("C03");

    // 3. Assert
    assertEquals(expectedResult, actualResult);
  }

  @Test
  public void should_throw_InputMismatchException_when_input_seatNo_end_with_lessThan01() {

    // 2. Actual
    Executable actualResult = () -> {
      seatService.reserve("A00");
    };

    // 3. Assert
    assertThrows(InputMismatchException.class, actualResult);
  }

  @Test
  public void should_return_status_is_0_when_input_seatNo_start_with_a_to_c_lower_case() {

    int expectedResult = 0;

    // 2. Actual
    int actualResult = seatService.reserve("b01");

    // 3. Assert
    assertEquals(expectedResult, actualResult);
  }

//    @Test
//    public void should_return_String_when_call_save_method_inSeatRepository() {
//         1. Arrange
//        Seat seat = new Seat();
//        when(seatServiceMock.save(any(Seat.class))).thenReturn("Your reservation is successfully");
//        String expectedResult = "Your reservation is successfully";
//
//         2. Actual
//        String actualResult = seatServiceMock.save(seat);
//
//         3. Assert
//        assertEquals(expectedResult, actualResult);
//    }
}
